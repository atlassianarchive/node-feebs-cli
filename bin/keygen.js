var spawn = require('child_process').spawn
  , read = require('read')
  , fs = require('fs')
  , path = require('path')
  , render = require(path.join(__dirname, 'templates'));

exports = module.exports = function(relPath){

  if(!relPath) relPath = ".";

  // process.on('uncaughtException', function(err) {
  //   console.log(render('openssl-does-not-exist'));
  // });

  var opensslCheck = spawn('openssl',['genrsa']);
  opensslCheck.on('close', function(code){
    if (code === 0)
      main();
  });

  function main(){
    fs.exists(path.join(relPath,'private-key.pem'), function (exists) {
      if (exists)
        promptOverwrite();
      else
        generateKeys();
    });
  }

  function promptOverwrite(){
    read({
      prompt: render('keys-exist-overwrite'),
      default: "n"
    }, function(er, overwrite){
      if(overwrite.toLowerCase() === "y")
        generateKeys();
    });
  }

  function generateKeys(){
    var genPriv = spawn("openssl", ['genrsa','-out',path.join(relPath,'private-key.pem'),'2048']);
    genPriv.on('close',function(code){
      if(code === 0) {
        var genPub = spawn('openssl',['rsa','-in',path.join(relPath,'private-key.pem'),
          '-pubout','-out',path.join(relPath,'public-key.pem')]);
        genPub.on('close',function(code){
          console.log(render('keygen-successful'));
        });
      }
    });
  }

}